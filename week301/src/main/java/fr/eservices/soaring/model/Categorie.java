package fr.eservices.soaring.model;

import javax.persistence.*;

@Entity
public class Categorie {
	@Id
	private int id;
	private String titre;

	public String getTitre() {
		return titre;
	}

	public void setTitre(String titre) {
		this.titre = titre;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
}
