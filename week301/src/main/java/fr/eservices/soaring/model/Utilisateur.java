package fr.eservices.soaring.model;
import javax.persistence.*;

@Entity
public class Utilisateur {
	@Id
	private String identifiant;
	private String motDePasse;
	private String nom;
	private String prenom;
	@ManyToOne
	private Profil profil_id;
	
	
	public String getIdentifiant() {
		return identifiant;
	}
	public void setIdentifiant(String identifiant) {
		this.identifiant = identifiant;
	}
	public String getMotDePasse() {
		return motDePasse;
	}
	public void setMotDePasse(String motDePasse) {
		this.motDePasse = motDePasse;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public Profil getProfil_id() {
		return profil_id;
	}
	public void setProfil_id(Profil profil_id) {
		this.profil_id = profil_id;
	}
	
	
}
