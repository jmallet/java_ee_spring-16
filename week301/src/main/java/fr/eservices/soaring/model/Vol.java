package fr.eservices.soaring.model;
import javax.persistence.*;

@Entity
public class Vol {
	@Id
	private int id;
	private String date;
	private int heureDecollage;
	private int heureAtterrissage;
	
	@ManyToOne
	private Pilote pilote_id;
	@ManyToOne
	private Epreuve epreuve_id;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public int getHeureDecollage() {
		return heureDecollage;
	}
	public void setHeureDecollage(int heureDecollage) {
		this.heureDecollage = heureDecollage;
	}
	public int getHeureAtterrissage() {
		return heureAtterrissage;
	}
	public void setHeureAtterrissage(int heureAtterrissage) {
		this.heureAtterrissage = heureAtterrissage;
	}
	public Pilote getPilote_id() {
		return pilote_id;
	}
	public void setPilote_id(Pilote pilote_id) {
		this.pilote_id = pilote_id;
	}
	public Epreuve getEpreuve_id() {
		return epreuve_id;
	}
	public void setEpreuve_id(Epreuve epreuve_id) {
		this.epreuve_id = epreuve_id;
	}
	
	
}
