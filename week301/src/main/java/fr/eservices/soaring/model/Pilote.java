package fr.eservices.soaring.model;
import javax.persistence.*;

@Entity
public class Pilote {
	@Id
	private int id;
	private String nom;	
	private String prenom;
	private String dateNaissance;
	private String adresse;
	private int codePostal;
	private String ville;
	private int telPortable;
	@ManyToOne
	private Club club_id;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Club getClub_id() {
		return club_id;
	}
	public void setClub_id(Club club_id) {
		this.club_id = club_id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public String getDateNaissance() {
		return dateNaissance;
	}
	public void setDateNaissance(String dateNaissance) {
		this.dateNaissance = dateNaissance;
	}
	public String getAdresse() {
		return adresse;
	}
	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}
	public int getCodePostal() {
		return codePostal;
	}
	public void setCodePostal(int codePostal) {
		this.codePostal = codePostal;
	}
	public String getVille() {
		return ville;
	}
	public void setVille(String ville) {
		this.ville = ville;
	}
	public int getTelPortable() {
		return telPortable;
	}
	public void setTelPortable(int telPortable) {
		this.telPortable = telPortable;
	}
	
}
