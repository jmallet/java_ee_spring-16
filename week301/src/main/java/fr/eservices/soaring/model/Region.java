package fr.eservices.soaring.model;
import javax.persistence.*;

@Entity
public class Region {
	@Id
	private int id;
	private String nom;

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
}
