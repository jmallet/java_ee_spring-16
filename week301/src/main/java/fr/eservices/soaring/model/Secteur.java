package fr.eservices.soaring.model;
import java.io.Serializable;

import javax.persistence.*;

@Entity
public class Secteur implements Serializable{
	private String rayon;
	private String type;
	@ManyToOne
	@JoinColumn
	@Id
	private Epreuve epreuve_id;
	@ManyToOne
	@JoinColumn
	@Id
	private PointPassage pointPassage_id;
	
	public String getRayon() {
		return rayon;
	}
	public void setRayon(String rayon) {
		this.rayon = rayon;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
}
