package fr.eservices.soaring.model;
import javax.persistence.*;

@Entity
public class Club {
	@Id
	private int id;
	private String nom;
	private String ville;
	@ManyToOne
	private Region region_id;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Region getRegion_id() {
		return region_id;
	}

	public void setRegion_id(Region region_id) {
		this.region_id = region_id;
	}

	public String getVille() {
		return ville;
	}

	public void setVille(String ville) {
		this.ville = ville;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

}
