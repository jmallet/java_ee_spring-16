package fr.eservices.soaring.model;
import javax.persistence.*;

@Entity
public class Produit {
	@Id
	private int id;
	private String libelle;
	private int prixUnitaire;
	@ManyToOne
	private Categorie categorie_id;
	
	public String getLibelle() {
		return libelle;
	}
	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}
	public int getPrixUnitaire() {
		return prixUnitaire;
	}
	public void setPrixUnitaire(int prixUnitaire) {
		this.prixUnitaire = prixUnitaire;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Categorie getCategorie_id() {
		return categorie_id;
	}
	public void setCategorie_id(Categorie categorie_id) {
		this.categorie_id = categorie_id;
	}
	
}
