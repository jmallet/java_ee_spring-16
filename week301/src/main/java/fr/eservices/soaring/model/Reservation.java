package fr.eservices.soaring.model;
import java.io.Serializable;

import javax.persistence.*;

@Entity
public class Reservation implements Serializable{
	private int nbPersonnes;
	@ManyToOne
	@JoinColumn
	@Id
	private Pilote pilote_id;
	@ManyToOne
	@JoinColumn
	@Id
	private Repas repas_id;

	public int getNbPersonnes() {
		return nbPersonnes;
	}

	public void setNbPersonnes(int nbPersonnes) {
		this.nbPersonnes = nbPersonnes;
	}

	public Pilote getPilote_id() {
		return pilote_id;
	}

	public void setPilote_id(Pilote pilote_id) {
		this.pilote_id = pilote_id;
	}

	public Repas getRepas_id() {
		return repas_id;
	}

	public void setRepas_id(Repas repas_id) {
		this.repas_id = repas_id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((pilote_id == null) ? 0 : pilote_id.hashCode());
		result = prime * result + ((repas_id == null) ? 0 : repas_id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Reservation other = (Reservation) obj;
		if (pilote_id == null) {
			if (other.pilote_id != null)
				return false;
		} else if (!pilote_id.equals(other.pilote_id))
			return false;
		if (repas_id == null) {
			if (other.repas_id != null)
				return false;
		} else if (!repas_id.equals(other.repas_id))
			return false;
		return true;
	}
	
	
	
}
