package fr.eservices.soaring.dao;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import fr.eservices.soaring.model.Pilote;
import fr.eservices.soaring.model.Repas;

public class RegistrationDaoFactory implements RegistrationDao{

	private static EntityManager em;
	private static RegistrationDao rd;
	
	public void setEntityManager( EntityManager em ) {
		this.em = em;
}
	
	public static RegistrationDao createRegistrationDao() {
//		throw new RuntimeException("Not Yet implemented");
		// TODO : implement this method
		EntityManagerFactory emf = null;
			// create entity manager factory
		emf = Persistence.createEntityManagerFactory("myApp");
		em = emf.createEntityManager();
		
		if(rd==null) rd = new RegistrationDaoFactory();
		return rd;

			
	}

	@Override
	public List<Pilote> findPilotsByName(String nom) {
		// TODO Auto-generated method stub
			List<Pilote> resultat =
				em.createQuery("SELECT p FROM Pilote p WHERE nom = :n", Pilote.class)
				.setParameter("n", nom)
				.getResultList();
		  
		return resultat;
	}

	@Override
	public List<Pilote> findPilotsByRegion(int id_region) {
		// TODO Auto-generated method stub
		List<Pilote> resultat =
				em.createQuery("SELECT p FROM Pilote p, Region r, Club c WHERE c.region_id = r.id and p.club_id = c.id and r.id = :idr", Pilote.class)
				.setParameter("idr", id_region)
				.getResultList();
		  
		return resultat;
	}

	@Override
	public List<Pilote> findPilotsByClub(int id_club) {
		// TODO Auto-generated method stub
		List<Pilote> resultat =
				em.createQuery("SELECT p FROM Pilote p, Club c WHERE p.club_id = c.id and c.id = :idc", Pilote.class)
				.setParameter("idc", id_club)
				.getResultList();
		  
		return resultat;
	}

	@Override
	public List<Pilote> findPilotsBelow(Date currentDate, int age) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map<Repas, Integer> getAvailableSeatsForLunch(Date day, String time) {
		// TODO Auto-generated method stub
		return null;
	}
	
}
