package fr.eservices.week402.app;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;


// Set this class as a configuration class,
@Configuration
// scan fr.eservices.week402.ctrl for components
@ComponentScan("fr.eservices.week402.ctrl")
// enable spring web mvc
@EnableWebMvc
public class AppConfig {

	// Add a method to provide an InternalResourceViewResolver,
	// put views in /WEB-INF/views
	// all views would be some jsp
//	ApplicationContext ctx = new AnnotationConfigApplicationContext(Application.class);
//	
//	Application app = ctx.getBean(Application.class);
//	app.run();

}
