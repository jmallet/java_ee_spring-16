package fr.eservices.week402.ctrl;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller()
@RequestMapping(path="/hello")
public class HelloController {

	@RequestMapping(path="/world")
	public String sayHello(
			Model model, 
			@RequestParam("name")
			String name) 
	{
		model.addAttribute("message", "Hello " + name + " !");
		return "/sample.jsp";
	}
	
//	@RequestMapping(path="/world")
//	public String testHello() 
//	{
//		return "Hello";
//	}
	
}
