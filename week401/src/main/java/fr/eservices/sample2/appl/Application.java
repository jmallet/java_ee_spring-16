package fr.eservices.sample2.appl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

import fr.eservices.sample2.api.Greeter;
import fr.eservices.sample2.api.Printer;
import fr.eservices.sample2.api.Welcome;

//TODO Add annotation if required.
@Configuration
@ComponentScan(value="fr.eservices.sample2.impl")
public class Application {

	@Autowired
	@Qualifier("console")
	Welcome welcome;
	@Autowired
	@Qualifier("english")
	Greeter greeter;
	@Autowired
	@Qualifier("console")
	Printer printer;
	

	public Application() {
	}
	
	public void run() {
		String name = welcome.askName();
		String response = greeter.hello(name);
		printer.print( response );
	}
	
	public static void main(String[] args) {
		// TODO Create a spring context
//		ApplicationContext ctx = new AnnotationConfigApplicationContext(Application.class);
		ApplicationContext ctx = new ClassPathXmlApplicationContext("application-context.xml");
		// TODO Get Application From context
		Application app = ctx.getBean(Application.class);
		// TODO Call run
		app.run();
		
	}
	
	public void setWelcome(Welcome welcome) {
		this.welcome = welcome;
	}

	public void setGreeter(Greeter greeter) {
		this.greeter = greeter;
	}

	public void setPrinter(Printer printer) {
		this.printer = printer;
	}
}
